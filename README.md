## Information

This is the repository for GLTF Packer.

For information, visit its webpage here: https://aiaicapta.in/gltf-packer/ 

## Disclaimer

GLTF Packer is in no way affiliated with KhronosGroup, glTF, Linden Lab, SecondLife or OpenSim. It is third party software
