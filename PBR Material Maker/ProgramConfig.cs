﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PBR_Material_Maker
{
    public class ProgramConfig
    {
        [JsonProperty]
        public int WindowHeight = 1030;

    }
}
